# zk

## Merging process

This repository follows the Git Flow branching strategy.

To propose a new feature:
- create a feature branch
- add your changes
- make a MR into `dev`