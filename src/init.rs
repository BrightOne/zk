use std::path::PathBuf;
use std::{fs, io};

use tracing::{info, instrument};

use crate::{config, constants, template};

#[instrument(err)]
pub fn init(Options { site_dir }: &Options) -> Result<(), Error> {
    fs::create_dir_all(site_dir).map_err(Error::CreateSiteDir)?;

    let config = config::Config::default();
    config
        .write(site_dir.join(constants::DEFAULT_CONFIG_NAME))
        .map_err(Error::CreateDefaultConfig)?;

    let content_dir = site_dir.join(constants::DEFAULT_CONTENT_DIR);
    fs::create_dir_all(&content_dir).map_err(Error::CreateContentDir)?;

    let index_file = content_dir.join(constants::DEFAULT_INDEX_FILE_NAME);
    let mut index_file = fs::File::create(&index_file).map_err(Error::CreateIndexFile)?;

    template::render(template::Index, &mut index_file).map_err(Error::RenderIndexTemplate)?;

    info!("success");
    Ok(())
}

#[derive(Debug, structopt::StructOpt)]
pub struct Options {
    /// Path to directory to be created
    site_dir: PathBuf,
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to create site directory")]
    CreateSiteDir(#[source] io::Error),
    #[error("failed to create default config")]
    CreateDefaultConfig(#[source] config::Error),
    #[error("failed to create content directory")]
    CreateContentDir(#[source] io::Error),
    #[error("failed to create index file")]
    CreateIndexFile(#[source] io::Error),
    #[error("failed to render index template")]
    RenderIndexTemplate(#[from] template::Error),
}
