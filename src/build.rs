use std::path::PathBuf;
use std::{fs, io};

use tracing::instrument;

use crate::constants;

#[instrument(err)]
pub fn build() -> Result<(), Error> {
    todo!("https://github.com/hackstream/zettel/blob/master/cmd/zettel/build.go#L76");
}

fn make_dist() -> Result<(), Error> {
    let dist_dir = PathBuf::from(constants::DEFAULT_DIST_DIR);
    fs::remove_dir_all(&dist_dir).map_err(Error::RemoveDistDir)?;
    let dirs = [
        dist_dir.clone(),
        dist_dir.join("css"),
        dist_dir.join("images"),
        dist_dir.join("data"),
        dist_dir.join("posts"),
        dist_dir.join("tags"),
    ];
    for dir in dirs.iter() {
        fs::create_dir_all(dir).map_err(|source| Error::CreateDir {
            dir: dir.clone(),
            source,
        })?;
    }

    let options = fs_extra::dir::CopyOptions {
        content_only: true,
        ..fs_extra::dir::CopyOptions::default()
    };
    fs_extra::dir::copy("templates/layouts", dist_dir, &options).map_err(Error::CopyDist)?;

    Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to create dist/{dir} directory")]
    CreateDir { dir: PathBuf, source: io::Error },
    #[error("failed to copy files in dist")]
    CopyDist(#[from] fs_extra::error::Error),
    #[error("failed to remove previous dist directory")]
    RemoveDistDir(#[source] io::Error),
}
