pub const DEFAULT_CONFIG_NAME: &str = "zettel.toml";
pub const DEFAULT_CONTENT_DIR: &str = "content";
pub const DEFAULT_DIST_DIR: &str = "dist";
pub const DEFAULT_INDEX_FILE_NAME: &str = "index.md";
pub const MAX_TITLE_LENGTH: usize = 20;
