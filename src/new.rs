use std::{fs, io};

use tracing::{info, instrument};

use crate::{constants, models, template};

#[instrument(err)]
pub fn new(Options { title }: &Options) -> Result<(), Error> {
    let current_dir = std::env::current_dir().map_err(Error::CurrentDir)?;
    let post = &models::Post::new(title);
    let path = current_dir
        .join(constants::DEFAULT_CONTENT_DIR)
        .join(&post.meta.title)
        .with_extension("md");
    let mut file = fs::File::create(&path).map_err(Error::CreateFile)?;
    template::render(template::Post { post }, &mut file).map_err(Error::Template)?;

    info!(path = %path.display(), "created");
    Ok(())
}

#[derive(Debug, structopt::StructOpt)]
pub struct Options {
    /// Title of the new post
    title: String,
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to get current directory")]
    CurrentDir(#[source] io::Error),
    #[error("failed to create post file")]
    CreateFile(#[source] io::Error),
    #[error("failed to render template")]
    Template(#[from] template::Error),
}
