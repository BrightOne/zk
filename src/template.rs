use std::io::{self, Write};

use askama::Template;

use crate::models;

#[derive(Template)]
#[template(path = "index.md")]
pub struct Index;

#[derive(Template)]
#[template(path = "post.md")]
pub struct Post<'a> {
    pub post: &'a models::Post,
}

pub fn render(template: impl Template, writer: &mut dyn Write) -> Result<(), Error> {
    let rendered = template.render().map_err(Error::Render)?;
    writer
        .write_all(rendered.as_bytes())
        .map_err(Error::Write)?;
    Ok(())
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("failed to render template")]
    Render(#[from] askama::Error),
    #[error("failed to write rendered template")]
    Write(#[from] io::Error),
}
