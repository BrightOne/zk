use rust_embed::RustEmbed;

#[derive(RustEmbed)]
#[folder = "templates/layouts/"]
pub struct Asset;
