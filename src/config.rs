use std::path::Path;
use std::{fs, io};

use serde::{Deserialize, Serialize};
use tracing::{debug, instrument};

use crate::constants;

#[derive(Debug, Deserialize, Serialize)]
pub struct Config {
    site_name: String,
    description: String,
    #[serde(rename = "pygmentsstyle")]
    pygments_style: String,
    site_prefix: Option<String>,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            site_name: "My Zettel".to_string(),
            description: "Hello World. This is my zettel notebook".to_string(),
            pygments_style: "monokailight".to_string(),
            site_prefix: None,
        }
    }
}

impl Config {
    #[instrument(err)]
    pub fn load() -> Result<Self, Error> {
        let mut config = config::Config::default();
        debug!(path = constants::DEFAULT_CONFIG_NAME, "reading config");
        config
            .merge(config::File::with_name(constants::DEFAULT_CONFIG_NAME))
            .map_err(Error::Read)?;
        Ok(config.try_into().map_err(Error::Parse)?)
    }

    pub fn write(&self, path: impl AsRef<Path>) -> Result<(), Error> {
        let toml = toml::to_string_pretty(self).map_err(Error::Serialize)?;
        fs::write(path, toml.as_bytes()).map_err(Error::Write)?;
        Ok(())
    }
}

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("reading config failed")]
    Read(#[source] config::ConfigError),
    #[error("parsing config failed")]
    Parse(#[source] config::ConfigError),
    #[error("serializing config failed")]
    Serialize(#[from] toml::ser::Error),
    #[error("writing config failed")]
    Write(#[from] io::Error),
}
