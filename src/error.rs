#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("config error")]
    Config(#[from] crate::config::Error),
    #[error("init error")]
    Init(#[from] crate::init::Error),
}
