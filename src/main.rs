use structopt::StructOpt;
use tracing::debug;

use zk::{build, init, new};
fn main() -> anyhow::Result<()> {
    let app = App::from_args();
    init_logger(app.verbose);
    debug!(?app, "start");

    match app.command {
        Command::Init(options) => init::init(&options)?,
        Command::New(options) => new::new(&options)?,
        Command::Build => build::build()?,
    };

    Ok(())
}

#[derive(Debug, StructOpt)]
struct App {
    #[structopt(long, short)]
    verbose: bool,
    #[structopt(subcommand)]
    command: Command,
}

#[derive(Debug, StructOpt)]
enum Command {
    Build,
    /// Initialize a new site with default config
    Init(init::Options),
    New(new::Options),
}

fn init_logger(verbose: bool) {
    use tracing_subscriber::filter::{EnvFilter, LevelFilter};
    let default_log_level = if verbose {
        LevelFilter::DEBUG
    } else {
        LevelFilter::INFO
    };
    let env_filter = EnvFilter::from_default_env().add_directive(default_log_level.into());
    tracing_subscriber::fmt()
        .with_env_filter(env_filter)
        .with_target(false)
        .init();
    debug!("logging initialized");
}
