use std::path::PathBuf;

use chrono::{DateTime, Utc};

use crate::constants;

#[derive(Debug, Default, Clone)]
pub struct Post {
    pub meta: Metadata,
    pub body: String,
    pub file_path: PathBuf,
    pub links: Vec<Link>,
    // pub connections: Vec<Link>,
}

impl Post {
    pub fn new(title: &str) -> Self {
        let mut post = Self::default();
        post.meta.title = Self::sanitize(title);
        post
    }

    pub fn slug(&self) -> String {
        self.file_path
            .with_extension("")
            .file_name()
            .and_then(|filename| filename.to_str())
            .unwrap()
            .to_string()
    }

    fn sanitize(title: &str) -> String {
        let mut title = title.to_lowercase().replace(' ', "-");
        title.truncate(constants::MAX_TITLE_LENGTH);
        title
    }
}

#[derive(Debug, Clone)]
pub struct Metadata {
    pub date: DateTime<Utc>,
    pub tags: Vec<String>,
    pub title: String,
    pub draft: bool,
}

impl Default for Metadata {
    fn default() -> Self {
        Self {
            date: Utc::now(),
            tags: Vec::new(),
            title: String::new(),
            draft: true,
        }
    }
}

#[derive(Debug, Default, Clone)]
pub struct Link {
    pub title: String,
    pub slug: String,
}
