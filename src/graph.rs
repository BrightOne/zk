use std::collections::BTreeMap;

use petgraph::prelude::*;
use serde::Serialize;

use crate::models::Post;

pub type PostGraph = DiGraph<Post, ()>;

// TODO: better than O(3n)?
pub fn make_graph(posts: &[Post]) -> PostGraph {
    let slugs = {
        let mut m = BTreeMap::new();
        for (i, post) in posts.iter().enumerate() {
            m.insert(post.slug(), i as u32);
        }
        m
    };
    let edges = posts.iter().enumerate().flat_map(|(i, post)| {
        post.links
            .iter()
            .map(|link| (i as u32, slugs[&link.slug]))
            .collect::<Vec<_>>()
    });
    let mut graph = PostGraph::from_edges(edges);
    for (i, post) in posts.iter().cloned().enumerate() {
        graph[NodeIndex::new(i)] = post;
    }
    graph
}

pub type Id = String;

#[derive(Debug, Serialize)]
pub struct GraphData {
    nodes: Vec<Node>,
    edges: Vec<Edge>,
}

#[derive(Debug, Serialize)]
pub struct Node {
    id: Id,
    label: String,
    size: u32,
    x: f64,
    y: f64,
}

#[derive(Debug, Serialize)]
pub struct Edge {
    id: Id,
    source: Id,
    target: Id,
}

impl From<PostGraph> for GraphData {
    fn from(post_graph: PostGraph) -> Self {
        let (nodes, edges) = post_graph.into_nodes_edges();
        let nodes = nodes
            .into_iter()
            .enumerate()
            .map(|(i, node)| Node {
                id: format!("nodeid-{}", i),
                label: node.weight.meta.title,
                size: 5,
                x: rand::random(),
                y: rand::random(),
            })
            .collect();
        let edges = edges
            .into_iter()
            .map(|edge| {
                // TODO: .index()?
                let (source, target) = (edge.source().index(), edge.target().index());
                Edge {
                    id: format!("edge-{}-{}", source, target),
                    source: format!("nodeid-{}", source),
                    target: format!("nodeid-{}", target),
                }
            })
            .collect();
        Self { nodes, edges }
    }
}
